# Website yaats.nl

## Development

Example to run Hugo from container with Podman or Docker.

```sh
podman run --rm -it -v $(pwd):/src:z -p 1313:1313 klakegg/hugo:latest-ext server
```

```sh
docker run --rm -it -v $(pwd):/src:z -p 1313:1313 klakegg/hugo:latest-ext server
```

Optional flags
```
server --disableFastRender --buildDrafts --cleanDestinationDir
```

### New page

```
docker run --rm -it -v $(pwd):/src:z -p 1313:1313 klakegg/hugo:latest-ext new docs/rustdesk.md
```
