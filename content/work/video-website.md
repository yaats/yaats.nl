---
title: "Zwemschool instructiewebsite"
url: 'projecten/video-website'
# date: 2022-07-06T00:00:00+01:00
draft: false
weight: 1
heroHeading: "Zwemschool instructiewebsite"
heroSubHeading: "Automatisch een website genereren op basis van video's in Vimeo"
heroBackground: 'https://res.cloudinary.com/yaats/image/upload/e_gradient_fade:20/v1657140347/yaats.nl/video-website.png'
thumbnail: 'https://res.cloudinary.com/yaats/image/upload/e_gradient_fade:20,q_auto,r_30/v1657140347/yaats.nl/video-website-2.png'
images: ['images/video-website-1.png', 'images/video-website-2.png', 'https://res.cloudinary.com/yaats/image/upload/e_sharpen:100,q_auto/v1657142179/yaats.nl/video-website-3.png', 'images/video-website.png']
---

Op basis van filmpjes in Vimeo wordt een website gemaakt met thumbnails, diverse metadata en daarop gebaseerde filters voor onder meer locatie, moeilijkheidsgraad en benodigd materiaal.

Via een touchscreen selecteert de aquasporter via een eenvoudig menu de juiste oefening. Een innovatie in het zwembad.

De zwemschool maakt instructievideo's en plaatst deze eventueel voorzien van een naam en omschrijving op Vimeo. In een eenvoudige webinterface test de zwemschool nieuwe versie van de website om deze vervolgens beschikbaar te maken voor de zwembaden. Op deze manier beheert de zwemschool eenvoudig meerdere instructiewebsites voor diverse zwembaden verspreid over de wereld.

Gebruikte technieken: #Docker #Hugo #OliveTin #Python
