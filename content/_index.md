---
title: 'Home'
date: 2018-02-12T15:37:57+07:00
heroHeading: 'Yaats'
heroSubHeading: 'Een eenmansbedrijf voor advies, beheer en ontwikkeling. Specialist op het gebied van Linux, Open Source en DevOps. Ontwikkeling en beheer van softwareoplossingen. Ruime ervaring met inrichten en beheren van ict-dienstverlening.'
heroBackground: 'images/serre-tafel-laptop-opac.jpg'
---
