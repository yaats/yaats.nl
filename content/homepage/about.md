---
title: 'Yaats'
weight: 2
background: ''
# button: 'About Us'
buttonLink: 'about'
---

Yaats is een eenmansbedrijf met diensten voor zzp'ers en particulieren.
Yaats levert advies, ontwikkeling en beheer op
gebied van kantoorautomatisering en webapplicaties.
Ruime ervaring met ontwikkeling van webapplicaties,
support en beheer van Windows en Linuxsystemen.
