---
title: "Online bestandsopslag"
date: 2022-07-08T17:10:26+02:00
icon: "services/synchronize.png"
featured: true
draft: false
weight: 500
heroHeading: 'Cloudopslag en synchronisatie'
heroSubHeading: 'Uw bestanden veilig online beschikbaar'
heroBackground: 'services/cloud.jpg'
---

Uw bestanden veilig online beschikbaar op telefoon, tablet en pc. Gemakkelijk bewaren, synchroniseren en delen van bestanden. Bestanden worden versleuteld opgeslagen en niet verwerkt of ingezien. U betaalt een eerlijke prijs en behoudt regie over uw data. 

#### Abonnementen

Beschikbare abonnementen bij Yaats.

{{<table table>}}
| Opslagruimte | Kosten*       | Domeinnaam   | Meerdere accounts |
|--------------|---------------|--------------|---------------------|
| 100 GB       | € 1,00 p.m.   | [stack.yaats-advies.nl](https://stack.yaats-advies.nl)  | - |
| 250 GB       | € 2,50 p.m.   | [stack.yaats-advies.nl](https://stack.yaats-advies.nl)  | - |
| 250 GB       | € 3,50 p.m.   | [een eigen domeinnaam]  | ✓  |
| 1.000 GB     | € 6,00 p.m    | [een eigen domeinnaam]  | ✓  |
| 2.000 GB     | € 11,00 p.m   | [een eigen domeinnaam]  | ✓  |
| * excl btw | | |
{{</table>}}

Bijkomende kosten betreffen eventueel een domeinnaam en ondersteuning bij gebruik.

{{< stack_form >}}

```

## TODO: aanmeldingsformulier / automatisering
```

### Stack

Yaats gebruikt voor deze dienst Stack van TransIP.

{{< stack_vergelijkingstabel >}}


### Nextcloud

Voor meer geavanceerde wensen: Nextcloud
