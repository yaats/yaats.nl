---
title: "Softwareontwikkeling"
date: 2022-07-06T09:54:44+02:00
icon: "services/coding.png"
featured: true
draft: true
weight: 100
heroHeading: 'Softwareontwikkeling'
heroSubHeading: 'Webapplicaties'
heroBackground: ''
---

Ontwikkeling, onderhoud en beheer van eenvoudige applicaties of services,
zoals aanwezigheids-, voortgangsregistratie of een e-mailnotificatie bij
een annulering van een automatische incasso.

Ervaring met talen en frameworks als Python, Javascript, Typescript
Angular, Vuejs en Hugo en tools als Git, Ansible, Docker en Podman containers.

