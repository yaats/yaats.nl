---
title: "Helpdesk"
date: 2022-07-08T17:11:15+02:00
icon: "services/remote-access.png"
featured: true
draft: false
weight: 400
heroHeading: 'Helpdesk'
heroSubHeading: 'Soms heb je gewoon even een specialist nodig'
heroBackground: ''
url: '/diensten/helpdesk'

---

Ondersteuning op afstand voor uw desktop, laptop, tablet of smartphone.
Laat een expert meekijken op afstand bij problemen of vragen. Deze dienst
is geschikt voor ondermeer particulieren en zzp'ers.

## Installatie en intake

Yaats installeert bij voorkeur zelf de app voor de ondersteuning op afstand.
U krijgt uitleg over de werking van de app.

Wanneer een installatie door Yaats niet niet haalbaar is,
kunt u zelf uw device voorzien van de Rustdeskapplicatie. De apps zijn via [deze pagina](https://stack.yaats-advies.nl/s/yKPrLB9g5FnTaVbv) beschikbaar.
- De app voor Windows is direct gebruiksklaar.
- De app voor iOS is via de store beschikbaar, maar is nog _niet_ geschikt om hulp op afstand te verkrijgen.
Dit vanwege restricties van Apple.
- De overige apps vereisen nog wat extra aanpassingen voor een optimale en veilige verbinding.

De intake bestaat uit het registreren van een aantal basisgegevens en verdere relevante informatie over uw systeem. Accountgevens en andere vertrouwelijke informatie worden _niet_ geregistreerd.

## Support

![image-right](/images/overwork.png)

Wanneer u support nodig heeft, maakt u online een afspraak.
U wordt vervolgens op afgesproken tijdstip teruggebeld.
Via de Rustdesk app kan Yaats meekijken op uw systeem en
aan de slag om uw probleem op te lossen.

## Veilig

![image-left](/services/remote-access.png)

Met Rustdesk heeft Yaats een oplossing gelecteerd die staat voor privacy en veiligheid.
Uw apparaten worden via een app aangesloten op een server van Yaats.
Dit betekent dat alleen systemen van Yaats uw device via deze app kunnen benaderen.
De gegevens die over deze verbinding verstuurd worden zijn bovendien versleuteld.
Hierdoor is er geen derde partij die zicht kan hebben op de verbinding.

## Rustdesk

Rustdesk is de app die gebruikt wordt voor het maken van de verbinding met uw apparaat.
Het kenmerk zich door:

- veiligheid door end-to-end encryptie
- privacy door decentralisering van de beheerservers
- cross-platform: beschikbaar voor desktop, laptop, tablet en smartphones
- open source licentie GNU AGPL v3.0

[![image](/images/rustdesk-logo-header.svg)](http://rustdesk.com/)

{{<table table>}}
| Kosten |   |
|----|---|
| Intake | €60 voor eerste apparaat, €30 voor elk extra apparaat |
| Support | €30 voor eerste half uur, €15 per extra kwartier |
| | prijzen excl. btw |
{{</table>}}
